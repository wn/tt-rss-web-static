# Email Digests

Users may opt into receiving daily (sent once every 24 hours) digests of unread headlines via email.

Digests are sent out by update daemon or while running <code>update.php —feeds</code>. Maximum of 15 messages is sent during one batch.

* To receive digests in HTML format install [PHPMailer plugin](https://git.tt-rss.org/fox/ttrss-mailer-smtp)
* Digests may be customized by editing ``templates/digest_template*.txt``.
* You can preview your digest contents in preferences.

